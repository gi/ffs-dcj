#define __STDCPP_WANT_MATH_SPEC_FUNCS__ 1
using namespace std;
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <random>
#include <stdlib.h>
#include <math.h>
#include <list>
#include <tuple>
#include <map>
#include <vector>
#include <stack>
#include <iostream>
#include <fstream>
#define DCJ_RATE 1.0
#define EXIT_ON_GENOME_FULL 1
#define CHR_LINEAR "|"
#define CHR_CIRCULAR ")"
#define TESTING 0




#define max_genes 40000
#define max_ext 2*max_genes+4

#define ZIPF_PRECISION 1e-10

random_device rd;  // Will be used to obtain a seed for the random number engine
mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
//mt19937 gen(10);
std::uniform_real_distribution<> uni_dis(0.0, 1.0);

//inclusive bounds
int rand_range(int lo, int hi) {
    //return rand()%(hi-lo+1) + lo;
    uniform_int_distribution<> distrib(lo, hi);
    return distrib(gen);
}

//uniform sampling between (0,1)
inline float rand_uniform() {
    return uni_dis(gen);
}

inline float zipf_density(int k,double shape) {
    return 1/(riemann_zeta(shape)*pow(k,shape));
}

void generate_zipf_probs(float* zpfs,int n, double shape) {
    for (int i=0; i< n+1; i++) {
        zpfs[i] = zipf_density(i,shape);
    }

}



//depends on the parameter of course, but for our tests 500 is enough
#define ZIPF_MAX_ITERATIONS 500
int zipf_naive(double shape) {
    float r = rand_uniform();
    int k=1;
    float remaining = 1.0-zipf_density(k,shape);
    //reverse because of float accuracy
    for (; k < ZIPF_MAX_ITERATIONS;) {
        if (remaining <= r) {
            return k;
        }
        remaining-=zipf_density(k++,shape);
    }
    cerr << "Broke off zipf simulation after "<<ZIPF_MAX_ITERATIONS << " iterations." << endl;
    return k;
}


inline int is_head(int e) {
    return e%2==0;
}
inline int is_tail(int e) {
    return e%2==1;
}


inline int is_cap(int e) {
    return e==0;
}

inline int head(int g) {
    return 2*g;
}

inline int tail(int g) {
    return 2*g-1;
}

inline int gene(int e) {
    return (e+1)/2;
}

inline int oriented_gene(int e) {
    return is_tail(e)? gene(e) : -gene(e);
}

tuple<int,int> extremities(int or_gene) {
    int hd = head(abs(or_gene));
    int tl = tail(abs(or_gene));
    tuple<int,int> fw,bw;
    fw={tl,hd};
    bw={hd,tl};
    return or_gene > 0? fw :bw;
}

inline int orientation(int or_gene) {
    return or_gene/abs(or_gene);
}

int other_ext(int e) {
    int g = gene(e);
    return is_head(e)? tail(g) : head(g);
}

string display_extremity(int x, int* families=0,bool print_fams=false) {
    if (is_cap(x)) return "$";
    if (print_fams) {
        return to_string(families[gene(x)])+(is_head(x)?"h":"t");
    }
    return to_string(gene(x))+(is_head(x)?"h":"t");
}


void print_extremity_array(int* ext_array, int curr_genes, int* families=0, bool print_fams=false) {
    for (int i=0; i <= head(curr_genes); i++) {
        int a = ext_array[i];
        cerr << display_extremity(i,families,print_fams) << " : " << display_extremity(a,families,print_fams) << endl;
    }

}

//int max_ext = head(max_genes);
#define MAX_RANDOM_EXT_ITERATIONS 1000000
int random_ext(int curr_genes, bool* deleted) {
    int curr_max_ext = head(curr_genes);
    int re;

    for (int i=0; i < MAX_RANDOM_EXT_ITERATIONS; i++) {
        re = rand_range(1,curr_max_ext);
        if (i%10 == 0) {
            cerr << "Reached "<< i << " iterations looking for extremity." << endl;
        }
        if (!deleted[gene(re)] && !is_cap(re)) {
            return re;
        }
    }
    cerr << "Maximum number of iterations exhausted looking for free random extremity." << endl;
    exit(1);
}


void perform_dcj(int* ext_arr,int a, int c, bool variant=false) {
    int b = ext_arr[a];
    int d = ext_arr[c];
    if (a==c || a==d) {
        //same adjacency -> perform single cut
        ext_arr[a]=0;
        ext_arr[c]=0;
        ext_arr[b]=0;
        ext_arr[d]=0;
        return;
    }
    if (!variant) {
        ext_arr[a] = c;
        ext_arr[b] = d;
        ext_arr[c] = a;
        ext_arr[d] = b;
    } else {
        ext_arr[a] = d;
        ext_arr[b] = c;
        ext_arr[c] = b;
        ext_arr[d] = a;
    }
    //reset telomeres if they were changed
    ext_arr[0] = 0;
}

void random_dcj(int* ext_arr, bool* deleted, int  curr_genes,int* families) {
    int a = random_ext(curr_genes,deleted);
    int c = random_ext(curr_genes,deleted);
    #if TESTING
    cerr << "Performing DCJ on adjacencies " << display_extremity(a,families) << display_extremity(ext_arr[a],families) << " " << display_extremity(c,families) << display_extremity(ext_arr[c],families) << endl;
    #endif
    perform_dcj(ext_arr,a,c,(rand_range(0,1)==1));
}

list<int> gene_sequence(int* ext_arr,int start,int length) {

    list<int> seq = {oriented_gene(start)};
    int curr = start;
    for (int i = 1; i < length; i++) {
        int other = other_ext(curr);
        curr = ext_arr[other];
        if (is_cap(curr) || curr==start) {
            //reached end of chromosome, either by telomere or by cycling around
            break;
        }
        seq.push_back(oriented_gene(curr));
    }
    return seq;
}

void output_list(list<int> l) {
    for (int i : l) {
        cerr << " " << i;
    }
    cerr << endl;
}


list<int>to_fams(list<int> l, int* families) {
    list<int> ret;
    for (int e : l) {
        ret.push_back(orientation(e)*families[abs(e)]);
    }
    return ret;
}


int random_deletion(int* ext_arr, bool* deleted, int  curr_genes, float indelshape,int* families) {
    int l = zipf_naive(indelshape);
    int e = random_ext(curr_genes,deleted);
    cerr << "Trying to delete segment of length " << l << endl;
    list<int> to_del = gene_sequence(ext_arr,e,l);
    #if TESTING ==1
    output_list(to_fams(to_del,families));
    #endif
    for (int dg : to_del) {
        deleted[abs(dg)] = true;
    }
    int fst_xt = get<0>(extremities(to_del.front()));
    int lst_xt = get<1>(extremities(to_del.back()));
    int a = ext_arr[fst_xt];
    int b = ext_arr[lst_xt];
    ext_arr[a] = b;
    ext_arr[b] = a;
    //circular excision, just for flavor; shouldn't matter, really
    ext_arr[fst_xt] = lst_xt;
    ext_arr[lst_xt] = fst_xt;
    return to_del.size();

}

pair<int,int> insert_segment(int* ext_arr,bool* deleted, int adj, int curr_genes, int* families, list<int> genes) {
    int a = adj;
    int b = ext_arr[adj];
    int newgene_start = orientation(genes.front())*(curr_genes+1);
    int newgene_end = orientation(genes.back())*(curr_genes+genes.size());
    int x_start = get<0>(extremities(newgene_start));
    int x_end = get<1>(extremities(newgene_end));
    ext_arr[a] = x_start;
    ext_arr[x_start] = a; 
    ext_arr[b] = x_end;
    ext_arr[x_end] = b;
    ext_arr[0] = 0;
    int prev = newgene_start;
    int i = 0;
    for (int g : genes) {
        int cur_gene = orientation(g)*(curr_genes+i+1);
        cerr << "Inserting " << g << " as " << cur_gene << endl;
        //set family to that of template
        families[abs(cur_gene)] = families[abs(g)];
        if (i==0) {
            i++;
            continue;
        }
        // set adjacency according to orientation of prev and current
        int s = get<1>(extremities(prev));
        int t = get<0>(extremities(cur_gene));
        ext_arr[s] = t;
        ext_arr[t] = s;
        prev = cur_gene;
        i++;
    }
    cerr << endl;
    return pair<int,int>(x_start,x_end);
}

/*
Check if num_genes can be added and if so, initialize them with their own families.
*/
bool reserve_space_for_genes(int num_genes, int* families,int curr_genes){
    if (curr_genes + num_genes >= max_genes) {
        return false;
    }
    for (int i = curr_genes+1; i < curr_genes+num_genes+1; i++) {
        families[i] = i;
    }
    return true;
}


int random_insertion(int* ext_arr, int curr_genes, int* families, bool* deleted, float indelshape) {
    int l = zipf_naive(indelshape);
    cerr << "Inserting segment of length " << l << endl;
    if (!reserve_space_for_genes(l,families,curr_genes)) {
        return -1;
    }
    int e = random_ext(curr_genes,deleted);
    list<int> random_gnz;
    for (int i=0; i < l; i++) {
        int o = rand_range(0,1)==1? -1: 1;
        random_gnz.push_back(o*(curr_genes+i+1));
    }
    #if TESTING
    cerr << "Inserting segment into " << display_extremity(e,families) << display_extremity(ext_arr[e], families) << endl;
    output_list(random_gnz);
    #endif
    insert_segment(ext_arr,deleted,e,curr_genes,families,random_gnz);
    return l;
}


int random_duplication(int* ext_arr, int curr_genes, int* families, bool* deleted, float dups_shape) {
    int l = zipf_naive(dups_shape);
    cerr << "Trying to duplicate segment of length " << l << endl;

    int e = random_ext(curr_genes,deleted);
    list<int> dp_gnz = gene_sequence(ext_arr,e,l);
    if (!reserve_space_for_genes(dp_gnz.size(),families,curr_genes)) {
        return -1;
    }
    int d = random_ext(curr_genes,deleted);
    #if TESTING
    cerr << "Duplicating segment into " << display_extremity(d,families) << display_extremity(ext_arr[d],families) << endl;
    output_list(to_fams(dp_gnz,families));
    #endif
    insert_segment(ext_arr,deleted,d,curr_genes,families,dp_gnz);
    return l;
}

list<int> get_telomeres(int* ext_arr,bool* deleted, int curr_genes) {
    list<int> tels;
    for (int i = 1; i <= head(curr_genes); i++) {
        if (is_cap(ext_arr[i])&&!deleted[gene(i)]) {
            tels.push_back(i);
        }
    }
    return tels;
}

void print_chromosomes(int* ext_arr, bool* deleted, int* families, int curr_genes,string genome="G",bool write_family_wise=true,ostream& strm = cerr) {
    strm << ">" << genome << endl;
    bool visited[curr_genes+1];
    for (int i=0; i < curr_genes+1; i++) {
        visited[i]=deleted[i];
    }
    visited[0] = false;
    for (int t : get_telomeres(ext_arr,deleted,curr_genes)) {
        //retrace linear chromosome starting at t
        int curr = t;
        if (visited[gene(t)]){
            continue;
        }
        do {
            int orient = orientation(oriented_gene(curr));
            if (write_family_wise) {
                strm << orient*families[gene(curr)] << " ";
            } else {
                strm << orient*gene(curr) << " ";
            }
            visited[gene(curr)] = true;
            curr = ext_arr[other_ext(curr)];
            if (visited[gene(curr)]) {
                cerr << "Something is seriously wrong. Already visited gene in linear chromosome." << endl;
                exit(1);
            }
        } while (!is_cap(curr));
        strm << CHR_LINEAR << endl;
    }
    //cerr << "Terminate outputting linear chromosomes" << endl;
    for (int i=1; i <= head(curr_genes);i++) {
        if (visited[gene(i)]) {
            continue;
        }
        int curr = i;
        do {
            int orient = orientation(oriented_gene(curr));
            if (write_family_wise) {
                strm << orient*families[gene(curr)] << " ";
            } else {
                strm << orient*gene(curr) << " ";
            }
            visited[gene(curr)] = true;
            curr = ext_arr[other_ext(curr)];
        } while (!visited[gene(curr)]);
        strm << CHR_CIRCULAR << endl;
    }
}

int simulate_wgd(int* ext_arr,bool* deleted, int curr_genes,int* families) {
    cerr << "Performing WGD." << endl;
    //copy the families of all markers
    bool visited[curr_genes+1];
    for (int i=0; i < curr_genes+1; i++) {
        visited[i]=deleted[i];
    }
    visited[0] =false;
    int interm_genes = curr_genes;
    for (int t : get_telomeres(ext_arr,deleted,curr_genes)) {
        //retrace linear chromosome starting at t
        int curr = t;
        list<int> lin_chr;
        if (visited[gene(t)]){
            continue;
        }
        do {
            int orient = orientation(oriented_gene(curr));
            visited[gene(curr)] = true;
            lin_chr.push_back(orient*(gene(curr)));
            curr = ext_arr[other_ext(curr)];
            if (visited[gene(curr)]) {
                cerr << "WGD: Something is seriously wrong. Already visited gene in linear chromosome." << endl;
                exit(1);
            }
        } while (!is_cap(curr));
        insert_segment(ext_arr,deleted,0,interm_genes,families,lin_chr);
        interm_genes+=lin_chr.size();
        ext_arr[0]=0;
        
    }
    for (int i=1; i <= head(curr_genes);i++) {
        list<int> circ_chr;
        if (visited[gene(i)]) {
            continue;
        }
        int curr = i;
        int start = curr;
        do {
            int orient = orientation(oriented_gene(curr));
            visited[gene(curr)] = true;
            circ_chr.push_back(orient*gene(curr));
            curr = ext_arr[other_ext(curr)];
        } while (!visited[gene(curr)]);
        //decide whether to just copy or self copy the chromosome
        if (rand_uniform()< 0.5) {
            insert_segment(ext_arr,deleted,ext_arr[start],interm_genes,families,circ_chr);
        } else {
            auto p = insert_segment(ext_arr,deleted,0,interm_genes,families,circ_chr);
            ext_arr[0]=0;
            ext_arr[p.first]=p.second;
            ext_arr[p.second] = p.first;
        }
        interm_genes+=circ_chr.size();
    }
    return interm_genes;

}


int simulate_ops(int* ext_arr,bool* deleted, int curr_genes,int* families,const int n_ops,float duprate=0.4,float dup_shape=4,float insrate=0.1,float delrate=0.2, float indelshape=6) {
    float all_rats = DCJ_RATE +duprate+insrate+delrate;
    float dcj_prob = DCJ_RATE/all_rats;
    float del_prob = delrate/all_rats;
    float ins_prob = insrate/all_rats;
    float dup_prob = duprate/all_rats;
    int deleted_genes = 0;
    cerr << "Starting simulation with the following probabilites:" << endl;
    cerr << "P(DCJ) = " << dcj_prob << endl;
    cerr << "P(Deletion) = " << del_prob << endl;
    cerr << "P(Insertion) =" << ins_prob << endl;
    cerr << "P(Duplication) = " << dup_prob << endl;
    float dcj_thresh = dcj_prob;
    float del_thresh = dcj_thresh+del_prob;
    float ins_thresh = del_thresh+ins_prob;
    for (int i = 0; i < n_ops; ++i) {
        #if TESTING
            print_chromosomes(ext_arr,deleted,families,curr_genes,"Before Step "+to_string(i));
        #endif
        float r = rand_uniform();
        cerr << "Round " << i << ", random var " << r << ". Deleted genes " << deleted_genes << " of " << curr_genes << endl;
        
        
        if (r < dcj_thresh) {
            cerr << "Performing DCJ." << endl;
            random_dcj(ext_arr,deleted,curr_genes,families);
        } else if (r < del_thresh) {
            cerr << "Performing Deletion." << endl;
            int l = random_deletion(ext_arr,deleted,curr_genes,indelshape,families);
            deleted_genes+=l;
            if (deleted_genes >= curr_genes) {
                cerr << "Genome has been depleted by deletions! Exiting..."<< endl;
                return -1;
            }
        } else if (r<ins_thresh) {
            cerr << "Performing Insertion." << endl;
            int l = random_insertion(ext_arr,curr_genes,families,deleted,indelshape);
            if (l==-1) {
                cerr << "Exceeded memory limit for insertions." << endl;
                #if EXIT_ON_GENOME_FULL
                    cerr << "Exiting..." << endl;
                    return -1;
                #endif
                }
             else {
                curr_genes+=l;
            }
        } else{
        cerr << "Performing Duplication." << endl;
        int l = random_duplication(ext_arr,curr_genes, families,deleted,dup_shape);
        if (l==-1) {
            cerr << "Exceeded memory limit for insertions." << endl;
            if (EXIT_ON_GENOME_FULL) {
                cerr << "Exiting..." << endl;
                return -1;
            }
        } else {
                curr_genes+=l;
        }
        }
        cerr << "alive" << endl;
    }
    cerr << "alivealive" << endl;
    return curr_genes;
}

void initialize_genome(int* ext_arr, int* families, int n_genes, int n_chroms, bool* deleted) {
    int chrom_sizes[n_chroms];
    //each chromosome needs at least one marker.
    for (int i = 0; i < n_chroms; i++) {
        chrom_sizes[i] = 1;
    }
    //distribute the rest of markers randomly
    for (int i = 0; i < n_genes - n_chroms; i++) {
        int r = rand_range(0,n_chroms-1);
        chrom_sizes[r]+=1;
    }
    cerr << "Generating root genome with the following sizes:" << endl;
    for (int i = 0; i < n_chroms; i++) {
        cerr << " " << chrom_sizes[i] << endl;
    }
    //build chromosomes
    //for locality reasons, we name the markers initially 1,2,3,...
    // this should in theory not affect results computed downstream
    int marker_name = 1;
    int prev_n_genes = 0;
    for (int i =0; i < n_chroms; i++) {
        list<int> chrom;
        int prev_n_genes = marker_name-1;
        for (int j=0; j < chrom_sizes[i];j++) {
            int orient = rand_range(0,1)==1?1:-1;
            chrom.push_back(orient*marker_name);
            marker_name++;
        }
        reserve_space_for_genes(marker_name-1,families,prev_n_genes);
        insert_segment(ext_arr,deleted,0,prev_n_genes,families,chrom);
    }

}

void init_deleted(bool* deleted,int sz){
    for (int i = 0; i < sz; i++) {
        deleted[i] = false;
    }
}

bool dummy_integrity(string s) {
    return true;
}
string dummy_wrong(string s) {
    return s+" is invalid.";
}


bool check_n_genes(string s) {
    int n_genes = stoi(s);
    if (to_string(n_genes)!=s) {
        return false;
    }
    if ( n_genes < 1 || n_genes > max_genes) {
        return false;
    }
    return true;
}

string wrong_n_genes(string s) {
    int n_genes = stoi(s);
    if (to_string(n_genes)!=s) {
        return "String "+s+" cannot be parsed to int.";
    }
    if ( n_genes == 0) {
        return "Genomes with 0 genes make no sense.";
    }
    if (n_genes < 0) {
        return "Negative number of genes? Seriously?";
    }
    if (n_genes > max_genes) {
        return "Sorry, cannot simulate more than "+to_string(max_genes)+" genes. Recompile with a higher limit on #max_genes.";
    }
    return "";
}

bool check_n_ops(string s) {
    int n_ops = stoi(s);
    if (to_string(n_ops)!=s) {
        return false;
    }
    if ( n_ops < 0 ) {
        return false;
    }
    return true;
}

string wrong_n_ops(string s) {
    int n_ops = stoi(s);
    if (to_string(n_ops)!=s) {
        return "String "+s+" cannot be parsed to int.";
    }
    if ( n_ops < 0 ) {
        return "Negative numbers of operations cannot be simulated.";
    }
    return "";
}

bool check_positive_real(string s) {
    try {
    float r = stof(s);
    return (r > 0);
    } catch (const invalid_argument e) {
        return false;
    }
}

string wrong_positive_real(string s) {
    try {
    float r = stof(s);
    if (r < 0) {
        return "Must be positive real number.";
    }
    return "";
    } catch (const invalid_argument e) {
        return "Couldn't parse "+s+" to float.";
    }
}

bool check_nn_real(string s) {
    try {
    float r = stof(s);
    return (r >= 0);
    } catch (const invalid_argument e) {
        return false;
    }
}

string wrong_nn_real(string s) {
    try {
    float r = stof(s);
    if (r <= 0) {
        return "Must be non-negative real number.";
    }
    return "";
    } catch (const invalid_argument e) {
        return "Couldn't parse "+s+" to float.";
    }
}

bool check_non_negative_real(string s){
    return false;
}

#define NARGS_PLUS -1
#define NARGS_STAR -2
#define NARGS_BOOL 0

class Argument {
    public:
        string internal_name;
        list<string> aliases;
        bool optional;
        int nargs;
        bool (*check_integrity) (string) = dummy_integrity;
        string (*whats_wrong) (string) = dummy_wrong;
        string out_name() {
            string ret = "";
            for (int i=0; i < internal_name.size(); i++) {
                char c = internal_name[i];
                if (c=='-') {
                    if (i > 2) {
                        ret+='_';
                    }
                    continue;
                }
                ret+=toupper(c);
            }
            return ret;
        }
        string narg_string() {
            string nm = out_name();
            switch (nargs)
            {
            case NARGS_BOOL:
                return " ";
                break;
            case NARGS_STAR:
                return " [ "+nm+" ["+nm+"...]]";
                break;
            case NARGS_PLUS:
                return " "+nm+" ["+nm+"...]";
                break;
            default:
                string ret = "";
                for (int i = 0; i < nargs; i++) {
                    ret+=" ";
                    ret+=nm;
                }
                return ret;
                break;
            }
        }
        string arg_str() {
            return internal_name;
        }
};

bool in_consume_range(int nargs,int offset) {
    if (nargs==NARGS_BOOL) {
        return false;
    }
    if (nargs==NARGS_PLUS || nargs== NARGS_STAR) {
        return true;
    }
    return (nargs >= offset);
}

bool fits_consume_range(int nargs,int act_num_args) {
    switch (nargs)
    {
    case NARGS_STAR:
        return true;
        break;
    case NARGS_PLUS:
        return act_num_args > 0;
        break;
    case NARGS_BOOL:
        return act_num_args == 0;
    default:
        return (act_num_args == nargs);
        break;
    }
}

#define NO_FIT -1
#define FIT_BUT_WRONG_NUM_ARGS -2
#define FIT_BUT_WRONG_ARG -3

int test_parse(vector<string> argv, int pos, Argument arg,list<Argument> other_args= {}) {
    int argc = argv.size();
    bool fits = false;
    for (string alias : arg.aliases) {
        fits = (alias==argv[pos]) || fits ;
    }
    if (!fits) {
        return NO_FIT;
    }
    list<string> vals;
    for (int i=pos+1; i < argc && in_consume_range(arg.nargs,i-pos); i++) {
        bool other_arg_start = false;
        for (Argument o_arg : other_args) {
            for (string o_alias : o_arg.aliases) {
                if (o_alias==argv[i]) {
                    other_arg_start=true;
                }
            }
        }
        if (other_arg_start){
            break;
        }
        if (!arg.check_integrity(argv[i])) {
            cerr << "Error parsing " << arg.arg_str() << ": " << arg.whats_wrong(argv[i]) << endl;
            return FIT_BUT_WRONG_ARG;
        }
        vals.push_back(argv[i]);
    }
    int nvals = vals.size();
    if (fits_consume_range(arg.nargs,nvals)) {
        return nvals;
    } else {
        return FIT_BUT_WRONG_NUM_ARGS;
    }
    
}

list<string> unsafe_parse(vector<string> argv, int pos, Argument arg,list<Argument> other_args= {}) {
    int argc = argv.size();
    list<string> vals;
    for (int i=pos+1; i < argc && in_consume_range(arg.nargs,i-pos); i++) {
        bool other_arg_start=false;
        for (Argument o_arg : other_args) {
            for (string o_alias : o_arg.aliases) {
                if (o_alias==argv[i]) {
                    other_arg_start=true;
                }
            }
        }
        if (other_arg_start){
            break;
        }
        vals.push_back(argv[i]);
    }
    return vals;
}

class ArgumentParser {
    vector<Argument> positional_arguments;
    vector<Argument> non_pos_arguments;
    map<string,list<string>> parsed_args;

    public:
    string program_title = "lies and deception (test version)";
    string created_by = "3 children in a trenchcoat";
    string header_tag = "=============================================";
    string footer_tag = "=============================================";
    void add_argument(string arg_name,list<string> aliases=list<string>(), bool optional=true,int nargs=1,bool (*check_integrity) (string)=dummy_integrity,string (*whats_wrong) (string) = dummy_wrong) {
        if (!(arg_name[0]=='-')) {
            //argument is positional
            //-> no aliases
            Argument arg;
            arg.internal_name = arg_name;
            arg.optional = false;
            arg.check_integrity = check_integrity;
            arg.whats_wrong = whats_wrong;
            arg.nargs = 1;
            positional_arguments.push_back(arg);

        } else {
            Argument arg;
            arg.internal_name = arg_name;
            arg.aliases = aliases;
            arg.check_integrity = check_integrity;
            arg.whats_wrong = whats_wrong;
            if (aliases.size() == 0){
                arg.aliases = {arg_name};
            }
            arg.optional = optional;
            arg.nargs = nargs;
            non_pos_arguments.push_back(arg);
        }
    }

    bool parse_args(int argc, char* argv[]) {
        vector<string> argvec;
        for (int i = 0; i< argc; i++) {
            argvec.push_back(argv[i]);
        }
        return parse_args(argvec);
    }
    bool parse_args(vector<string> argv) {
        int argc = argv.size();
        list<Argument>o_args;
        for (Argument arg : non_pos_arguments) {
            o_args.push_back(arg);
        }
        for (int i=1;i< argc; i++) {
            int pos_idx = i-1;
            if (pos_idx < positional_arguments.size()) {
                Argument p_arg = positional_arguments[pos_idx];
                parsed_args[p_arg.internal_name] = {argv[i]};
            } else {
                bool found_arg = false;
                Argument f_arg;
                for (Argument arg : non_pos_arguments) {
                    int cons_vals = test_parse(argv,i,arg,o_args);
                    if (cons_vals==FIT_BUT_WRONG_NUM_ARGS ) {
                        cerr << "Wrong number of arguments for argument " << argv[i] << ". Requires "<< arg.nargs << " argument(s)." << endl;
                        return false;
                    }
                    if (cons_vals==FIT_BUT_WRONG_ARG ) {
                        return false;
                    }
                    if (cons_vals >= 0) {
                        found_arg = true;
                        f_arg = arg;
                        break;
                    }
                }
                if (!found_arg) {
                    cerr << "Unrecognized argument " << argv[i] << endl;
                    return false;
                }
                list<string> l = unsafe_parse(argv,i,f_arg);
                parsed_args[f_arg.internal_name] = l;
                i+=(l.size());
            }
        }
        //check if all required arguments are satisfied
        for (Argument parg : positional_arguments) {
            if (!(parsed_args.count(parg.internal_name) > 0)) {
                return false;
            }
        }
        for (Argument nparg : non_pos_arguments) {
            if (!(parsed_args.count(nparg.internal_name) > 0)&& !nparg.optional) {
                return false;
            }
        }
        return true;
    }
    void print_usage(int argc,char* argv[]) {
        cerr << header_tag << endl;
        cerr << "This is " << program_title << " (" << argv[0] << "), created by " << created_by << "." << endl;
        cerr << " Usage: ";
        cerr << argv[0] << " ";
        for (auto parg : positional_arguments) {
            cerr << parg.internal_name << " ";
        }
        for (auto nparg: non_pos_arguments) {
            string flanka = " ";
            string flankb = " ";
            if (nparg.optional) {
                flanka = " [";
                flankb = "] ";
            }
            //cerr << "aelive" << nparg.aliases.front() << endl;
            cerr << flanka << nparg.arg_str() << nparg.narg_string() << flankb;
        }
        cerr << endl;
        cerr << footer_tag << endl;
    }
    map<string,list<string>> get_parsed_args() {
        return parsed_args;
    }
};




#define A_GENES "--genes"
#define A_CHRMS "--nchrms"
#define A_OPS "--nops"
#define R_OPS "--rops"
#define A_INS_RATE "--ins-rate"
#define A_DUP_RATE "--dup-rate"
#define A_DEL_RATE "--del-rate"
#define A_ID_ZIPF "--indel-size-zipf"
#define A_DUP_ZIPF "--dup-size-zipf"
#define A_HELP "--help"
#define NWK_TREE "--nwk"
#define NAME "--name"
#define WRITE_FILE "--wf"
#define PERFORM_WGD "--wgd"

void setup_args(ArgumentParser &parser) {
    parser.add_argument(A_GENES,{A_GENES,"-g"},false,1,check_n_genes,wrong_n_genes);
    parser.add_argument(A_OPS,{A_OPS,"-o"},false,1,check_n_ops,wrong_n_ops);
    parser.add_argument(A_CHRMS,{A_CHRMS,"-x"},true,1,check_n_genes,wrong_n_genes);
    parser.add_argument(A_INS_RATE,{A_INS_RATE,"-i"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_DUP_RATE,{A_DUP_RATE,"-d"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_DEL_RATE,{A_DEL_RATE,"-e"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_ID_ZIPF,{},true,1,check_positive_real,wrong_positive_real);
    parser.add_argument(A_DUP_ZIPF,{},true,1,check_positive_real,wrong_positive_real);
    parser.add_argument(A_HELP,{A_HELP,"-h"},true,NARGS_BOOL);
    parser.add_argument(NWK_TREE,{},true,1);
    parser.header_tag= "=============== Finally, a Fast Simulator for DCJs (...and indels/duplications) ===============";
    parser.footer_tag= "===============================================================================================";
    parser.program_title = "ffs-DCJ";
    parser.created_by = "Leonard Bohnenkaemper";

}

void setup_nwk_args(ArgumentParser &parser) {
    parser.add_argument(NAME,{},true,1);
    parser.add_argument(WRITE_FILE,{},true,NARGS_BOOL);
    parser.add_argument(PERFORM_WGD,{},true,NARGS_BOOL);
    parser.add_argument(R_OPS,{R_OPS,"-r"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_INS_RATE,{A_INS_RATE,"-i"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_DUP_RATE,{A_DUP_RATE,"-d"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_DEL_RATE,{A_DEL_RATE,"-e"},true,1,check_nn_real,wrong_nn_real);
    parser.add_argument(A_ID_ZIPF,{},true,1,check_positive_real,wrong_positive_real);
    parser.add_argument(A_DUP_ZIPF,{},true,1,check_positive_real,wrong_positive_real);
}



inline list<string> get_or_default(map<string,list<string>> mp,string key,list<string> df_list={}) {
    return (mp.count(key) > 0)? mp[key]: df_list;
 }


 

 void cpy_genome(int* t_ext_arr, int* t_families, bool* t_deleted, int curr_genes, int* c_ext_arr, int* c_families, bool* c_deleted) {
    for (int i = 0; i <= curr_genes; i++) {
        c_families[i] = t_families[i];
        c_deleted[i] = t_deleted[i];
        if (i>0) {
            int ti = tail(i);
            int hi = head(i);
            c_ext_arr[ti] = t_ext_arr[ti];
            c_ext_arr[hi] = t_ext_arr[hi];
        }
    }
 }

string nw_annotation(string& nw_str,int close_pos) {
    string ret = "";
    for (int i = close_pos+1; i < nw_str.length(); i++) {
        if (nw_str[i] == ',' || nw_str[i] == '(' || nw_str[i] == ')' || nw_str[i] == ';' ) {
            break;
        }
        ret+=nw_str[i];
    }
    return ret;
}

 pair<stack<pair<int,int>>,map<int,string>> parse_newick(string& nw_str) {
    stack<int> opens, children_open;
    stack<pair<int,int>> tree;
    map<int,string> annots;
    int j = 0;
    for(int i = 0; i < nw_str.length(); i++) {
        if (nw_str[i]=='(') {
            opens.push(i);
        }
        if (nw_str[i]=='(' || nw_str[i]==',') {
            if (nw_str[i+1]!='(') {
                children_open.push(i+1);
                annots[i+1] = nw_annotation(nw_str,i);
                cerr << "Annotation " << i+1 << " " << annots[i+1] << endl;
            }
        }
        if (nw_str[i]==')') {
            j=opens.top();
            opens.pop();
            bool is_child = children_open.size() > 0;
            //collect all children of this node
            while(is_child) {
                int cco = children_open.top();
                if (cco > j) {
                    children_open.pop();
                    tree.push(pair<int,int>(j,cco));
                    cerr << "Tree edge: " << j << " -> " << cco << endl;
                    is_child = children_open.size() > 0;
                } else {
                    is_child = false;
                }
            }
            //annotate the node
            annots[j] = nw_annotation(nw_str,i);
            cerr << "Annotation " << j << " " << annots[j] << endl;
            children_open.push(j);
        }
    }
    return pair<stack<pair<int,int>>,map<int,string>>(tree,annots);
}


vector<string> prepare_tree_args(string s) {
    vector<string> entries = {""};
    bool is_space = true;
    string curr = "";
    for (char c : s) {
        if(c==' ') {
            if (!is_space) {
                entries.push_back(curr);
                curr="";
                is_space=true;
            }
        } else {
            curr+=c;
            is_space=false;
        }
    }
    if (!is_space) {
        entries.push_back(curr);
    }
    return entries;
}

int main(int argc, char* argv[]) {
    auto parser = ArgumentParser();
    setup_args(parser);
    if (!parser.parse_args(argc,argv)) {
        parser.print_usage(argc,argv);
        return 1;
    }
    auto args = parser.get_parsed_args();
    if (args.count(A_HELP)> 0) {
        parser.print_usage(argc,argv);
        return 1;
    }
    //setup args
    int n_genes = stoi(args[A_GENES].front());
    int n_chrms = stoi(get_or_default(args,A_CHRMS,{"1"}).front());
    int n_ops = stoi(args[A_OPS].front());
    float ins_rate = stof(get_or_default(args,A_INS_RATE,{"0.1"}).front());
    float dup_rate = stof(get_or_default(args,A_DUP_RATE,{"0.4"}).front());
    float del_rate = stof(get_or_default(args,A_DEL_RATE,{"0.2"}).front());
    float id_zipf_shape = stof(get_or_default(args,A_ID_ZIPF,{"4.0"}).front());
    float dp_zipf_shape = stof(get_or_default(args,A_DUP_ZIPF,{"6.0"}).front());
    string nw_tree = "(--name A --rops 0.5 --wf,--name B --rops 0.5 --wf);";
    if (args.count(NWK_TREE) > 0) {
        ifstream nw_file;
        nw_file.open(args[NWK_TREE].front());
        getline(nw_file,nw_tree);
        nw_file.close();
    }
    auto parsed_tree = parse_newick(nw_tree);
    auto tree_struct = parsed_tree.first;
    auto annots = parsed_tree.second;

    map<int,int*> ext_arr_m;
    map<int,int*> families_m;
    map<int,bool*> deleted_m;
    map<int,int> n_genes_m;
    int root = 999;
    for (pair<int,string> p : annots) {
        root =  p.first < root? p.first : root;
    }
    ext_arr_m[root] = (int*) malloc(max_ext*sizeof(int));
    families_m[root] = (int*) malloc(max_genes*sizeof(int));
    deleted_m[root] = (bool*) malloc(max_genes*sizeof(bool)); 

    int* x_ptr = ext_arr_m[root];
    int* f_ptr = families_m[root];
    bool* d_ptr = deleted_m[root];

    init_deleted(deleted_m[root],max_genes);
    initialize_genome(ext_arr_m[root],families_m[root],n_genes,n_chrms,deleted_m[root]);
    cerr << "Finished building root genome." << endl;
    print_chromosomes(ext_arr_m[root],deleted_m[root],families_m[root],n_genes,"Root");
    n_genes_m[root] = n_genes;
    string annot = annots[root];
    ArgumentParser nwk_param_parser;
    setup_nwk_args(nwk_param_parser);
    nwk_param_parser.parse_args(prepare_tree_args(annot));
    map<string,list<string>> nw_args = nwk_param_parser.get_parsed_args();
    map<int,vector<int>> gene_tree_children;
    map<int,string> gene_tree_annotation;
    for (int i=1; i<= n_genes_m[root]; i++) {
        gene_tree_children[i] = {};
    }
    if (nw_args.count(WRITE_FILE)) {
            string gname = nw_args.count(NAME) > 0? nw_args[NAME].front():"Root";
            print_chromosomes(ext_arr_m[root],deleted_m[root],families_m[root],n_genes_m[root],gname,true,cout);
            print_chromosomes(ext_arr_m[root],deleted_m[root],families_m[root],n_genes_m[root],gname,false,cerr);

    }
    cerr << "Simulating tree of size " << tree_struct.size() << endl;
    while (!tree_struct.empty()) {
        //cerr << "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << endl;
        auto p = tree_struct.top();
        tree_struct.pop();
        int source = p.first;
        int target = p.second;
        ext_arr_m[target] = (int*) malloc(max_ext*sizeof(int));
        families_m[target] = (int*) malloc(max_genes*sizeof(int));
        deleted_m[target] = (bool*) malloc(max_genes*sizeof(bool));
        init_deleted(deleted_m[target],max_genes);
        int new_genes = n_genes_m[source];
        cpy_genome(ext_arr_m[source],families_m[source],deleted_m[source],new_genes,ext_arr_m[target],families_m[target],deleted_m[target]);
        string annot = annots[target];
        ArgumentParser nwk_param_parser;
        setup_nwk_args(nwk_param_parser);
        nwk_param_parser.parse_args(prepare_tree_args(annot));
        map<string,list<string>> nw_args = nwk_param_parser.get_parsed_args();
        cerr << "ARGUMENTS FOR THIS BRANCH" << endl;
        for (auto p : nw_args) {
            cerr << p.first << " : " << (p.second.size() > 0? p.second.front(): "*") << (p.second.size() > 1? "...":"") << endl;
        }
        if (nw_args.count(PERFORM_WGD)) {
            new_genes = simulate_wgd(ext_arr_m[target],deleted_m[target],new_genes,families_m[target]);
            if (new_genes < 0) {
                cerr << "Serious error in WGD" << endl;
                return 1;
            }
        }
        int thistimenops = n_ops*stof(get_or_default(nw_args,R_OPS,{"1.0"}).front());
        cerr << "THISTIMENOPS "<< thistimenops << endl <<endl;
        if (thistimenops>0) {
        int ngenes = simulate_ops(ext_arr_m[target],deleted_m[target],new_genes,families_m[target],
                                    thistimenops,
                                    nw_args.count(A_DUP_RATE) > 0?stof(nw_args[A_DUP_RATE].front()):dup_rate,
                                    nw_args.count(A_DUP_ZIPF) > 0?stoi(nw_args[A_DUP_ZIPF].front()):dp_zipf_shape,
                                    nw_args.count(A_INS_RATE) > 0?stof(nw_args[A_INS_RATE].front()):ins_rate,
                                    nw_args.count(A_DEL_RATE) > 0?stof(nw_args[A_DEL_RATE].front()):del_rate,
                                    nw_args.count(A_ID_ZIPF) > 0?stoi(nw_args[A_ID_ZIPF].front()):id_zipf_shape);
        n_genes_m[target]=ngenes;
        } else {
            n_genes_m[target]=new_genes;
        }
        
        cerr << "Finished simulating branch "<<source << " -> " << target << endl;
        if (nw_args.count(WRITE_FILE)) {
            string gname = nw_args.count(NAME) > 0? nw_args[NAME].front():"Unnamed"+target;
            print_chromosomes(ext_arr_m[target],deleted_m[target],families_m[target],n_genes_m[target],gname,true,cout);
            print_chromosomes(ext_arr_m[target],deleted_m[target],families_m[target],n_genes_m[target],gname,false,cerr);

        }
        
    }

    for (pair<int,int*> p : ext_arr_m) {
        free(p.second);
    }
    for (pair<int,int*> p : families_m) {
        free(p.second);
    }
    for (pair<int,bool*> p : deleted_m) {
        free(p.second);
    }
    return 0;
    
    /*

    int a_ops = n_ops/2;
    int b_ops = (n_ops+1)/2; 
    cerr << "Simulating A branch with " << a_ops << " operations." << endl;
    int a_n_genes = simulate_ops(a_ext_arr,a_deleted,n_genes,a_families,a_ops,dup_rate,dp_zipf_shape,ins_rate,del_rate,id_zipf_shape);
    cerr << "Complete simulating A." << endl;
    if (a_n_genes < 0){
        cerr << "Something went wrong. Exiting..." << endl;
        return 1;
    }
    cerr << "Simulating B branch with " << b_ops << " operations." << endl;
    int b_n_genes = simulate_ops(b_ext_arr,b_deleted,n_genes,b_families,b_ops,dup_rate,dp_zipf_shape,ins_rate,del_rate,id_zipf_shape);
    if (b_n_genes < 0){
        cerr << "Something went wrong. Exiting..." << endl;
        return 1;
    }
    cerr << "Complete simulating B." << endl;
    print_chromosomes(a_ext_arr,a_deleted,a_families,a_n_genes,"A",true,cout);
    print_chromosomes(b_ext_arr,b_deleted,b_families,b_n_genes,"B",true,cout);

    return 0;
    */
}
